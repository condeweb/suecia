class Person < ApplicationRecord
  belongs_to :group, optional: true
  belongs_to :title, optional: true
  belongs_to :firm, optional: true
  belongs_to :position, optional: true

  validates :names, presence: true
  validates :last_names, presence: true
  validates :email, presence: true
end
