class Invitation < ApplicationRecord
  belongs_to :person
  belongs_to :event
  belongs_to :state
  validates :person, uniqueness: { scope: :event }

end
