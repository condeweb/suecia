class InvitationsController < ApplicationController
	before_action :authenticate_user!
	def generate
		e=params[:event_id]
		Person.all.each do |person|
			if !Invitation.exists?(event_id:e, person_id: person.id)
				t = generate_token
				i=Invitation.create(person_id: person.id,event_id:e,state_id:1,token_person:t)
				InvitationMailer.welcome_email(person,t).deliver_now
			end

		end
	end

	private

	def generate_token
	    loop do
	      token = SecureRandom.hex(10)
	      break token unless Invitation.where(token_person: token).exists?
	    end
  	end
end
