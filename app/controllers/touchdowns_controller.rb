class TouchdownsController < ApplicationController
	before_action :authenticate_user!
	
	def index
		@token = params[:token] || ""
		@event = Invitation.where(token_person:@token).last if Invitation.where(token_person:@token).present?

		if @event.update(state_id: 4)
			@state="Confirmado"
		else
			@state="Error"
		end
	end
end
