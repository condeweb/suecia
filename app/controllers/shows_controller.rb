class ShowsController < ApplicationController
	def index
		@token = params[:token] || ""
		@event = Invitation.where(token_person:@token).last.event if Invitation.where(token_person:@token).present?
	end

	def confirm
		@token = params[:token] || ""
		if Invitation.where(token_person:@token).where(state_id:1).present?
		 	i = Invitation.where(token_person:@token).where(state_id:1).last
		 	if i.update(state_id: 2)
		 		
		 		
		 		InvitationMailer.confirm_email(i.person,i).deliver_now
		 		
		 		respond_to do |format|
				    msg = { :status => "ok", :message => "Success!", :html => "<b>Bient</b>" }
				    format.json  { render :json => msg } # don't do msg.to_json
				end
		 	else
		 		respond_to do |format|
				    msg = { :status => "fail", :message => "Unsuccess!", :html => "<b>Mal</b>" }
				    format.json  { render :json => msg } # don't do msg.to_json
				end
		 	end
		else
			respond_to do |format|
			    msg = { :status => "fail", :message => "Unsuccess!", :html => "<b>Mal</b>" }
			    format.json  { render :json => msg } # don't do msg.to_json
			end
		end 
	end
	def reject
		@token = params[:token] || ""
		if Invitation.where(token_person:@token).where(state_id:1).present?
		 	i = Invitation.where(token_person:@token).where(state_id:1)
		 	if i.update(state_id: 3)

		 		respond_to do |format|
				    msg = { :status => "ok", :message => "Success!", :html => "<b>Bient</b>" }
				    format.json  { render :json => msg } # don't do msg.to_json
				end
		 	else
		 		respond_to do |format|
				    msg = { :status => "fail", :message => "Unsuccess!", :html => "<b>Mal</b>" }
				    format.json  { render :json => msg } # don't do msg.to_json
				end
		 	end
		else
			respond_to do |format|
			    msg = { :status => "fail", :message => "Unsuccess!", :html => "<b>Mal</b>" }
			    format.json  { render :json => msg } # don't do msg.to_json
			end
		end 
	end
end
