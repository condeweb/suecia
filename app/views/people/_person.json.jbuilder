json.extract! person, :id, :names, :last_names, :group_id, :title_id, :firm_id, :position_id, :email, :created_at, :updated_at
json.url person_url(person, format: :json)
