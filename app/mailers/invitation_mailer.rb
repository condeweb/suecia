class InvitationMailer < ApplicationMailer
	def welcome_email(user,token)
    	@user = user
    	@token = token
    	mail(to: @user.email, subject: 'Welcome to My Awesome Site')
  	end
  	def confirm_email(user,invitation)
    	@user = user
    	@invitation = invitation
      create_qr(invitation)
      attachments.inline['qr.png'] = File.read("tmp/#{invitation.token_person}.png")
      if mail(to: @user.email, subject: 'Confirm Invitation')
        delete_qr(invitation)
      end
      
  	end
  private

  def create_qr(invitation)
    qrcode = RQRCode::QRCode.new("http://localhost/touchdowm?token=#{invitation.token_person}")
    png = qrcode.as_png(
      resize_gte_to: false,
      resize_exactly_to: false,
      fill: 'white',
      color: 'black',
      size: 800,
      border_modules: 4,
      module_px_size: 6,
      file: nil # path to write
    )
    png.save("tmp/#{invitation.token_person}.png", :interlace => true)
    
  end

  def delete_qr(invitation)
    File.delete("tmp/#{invitation.token_person}.png") if File.exist?("tmp/#{invitation.token_person}.png")
  end
end
