Rails.application.routes.draw do
  devise_for :users
  devise_for :models
  resources :events
  resources :people
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html


	post '/g_invitation', to: 'invitations#generate'



	get '/show', to: 'shows#index'

	post '/shows', to: 'shows#confirm'

	post '/showsc', to: 'shows#reject'

	get '/touchdown', to: 'touchdowns#index'


	root to: "events#index"

end
