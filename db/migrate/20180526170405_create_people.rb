class CreatePeople < ActiveRecord::Migration[5.0]
  def change
    create_table :people do |t|
      t.string :names
      t.string :last_names
      t.references :group, foreign_key: true
      t.references :title, foreign_key: true
      t.references :firm, foreign_key: true
      t.references :position, foreign_key: true
      t.string :email

      t.timestamps
    end
  end
end
