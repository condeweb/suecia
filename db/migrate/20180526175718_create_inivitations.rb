class CreateInivitations < ActiveRecord::Migration[5.0]
  def change
    create_table :inivitations do |t|
      t.references :person, foreign_key: true
      t.references :event, foreign_key: true
      t.references :state, foreign_key: true

      t.timestamps
    end
  end
end
